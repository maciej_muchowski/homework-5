package pl.codementors.contacts;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/view/contacts.fxml"));

        Scene scene = new Scene(root, 600, 600);
        stage.setScene(scene);
        stage.show();

        stage.setOnCloseRequest(event -> {
            ContactsEntityManagerFactory.close();
            Platform.exit();
        });
    }

    public static void main(String[] args) {
        launch(args);
    }
}
