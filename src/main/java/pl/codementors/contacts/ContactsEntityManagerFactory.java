package pl.codementors.contacts;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class ContactsEntityManagerFactory {
    private static final EntityManagerFactory emf = Persistence.createEntityManagerFactory("PersonsPU");

    public static final EntityManager createEntityManager(){
        return emf.createEntityManager();
    }

    public static void close(){
        emf.close();
    }
}
