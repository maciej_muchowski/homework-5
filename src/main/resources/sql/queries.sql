CREATE DATABASE contacts;
ALTER DATABASE contacts CHARACTER SET utf8 COLLATE utf8_general_ci;
CREATE USER 'admin' IDENTIFIED BY 'password';
GRANT ALL ON contacts.* TO 'admin';
USE contacts;
CREATE TABLE genders(
gender VARCHAR(10) PRIMARY KEY
);
CREATE TABLE persons(
id INT PRIMARY KEY AUTO_INCREMENT,
first_name VARCHAR(32),
last_name VARCHAR(255),
phone_number VARCHAR(32),
gender VARCHAR(10)
);
ALTER TABLE persons ADD FOREIGN KEY (gender) REFERENCES genders (gender);
CREATE TABLE interests(
id INT PRIMARY KEY AUTO_INCREMENT,
name VARCHAR(255)
);
CREATE TABLE persons_interests(
person_id INT NOT NULL,
interest_id INT NOT NULL
);
ALTER TABLE persons_interests ADD PRIMARY KEY (person_id,interest_id);
ALTER TABLE persons_interests ADD FOREIGN KEY (person_id) REFERENCES persons(id);
ALTER TABLE persons_interests ADD FOREIGN KEY (interest_id) REFERENCES interests(id);
INSERT INTO genders(gender) VALUES("MALE");
INSERT INTO genders(gender) VALUES("FEMALE");
