package pl.codementors.contacts;

import pl.codementors.contacts.model.Interest;
import pl.codementors.contacts.model.Person;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.List;

public class ContactsDao {
    public Person addPerson(Person person) {
        EntityManager em = ContactsEntityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.persist(person);
        tx.commit();
        em.close();
        return person;
    }
    public List<Person> findAllPersons() {
        EntityManager em = ContactsEntityManagerFactory.createEntityManager();
        List<Person> persons = em.createQuery("select p from Person p").getResultList();
        em.close();
        return persons;
    }

    public Person updatePerson(Person person) {
        EntityManager em = ContactsEntityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        person = em.merge(person);
        tx.commit();
        em.close();
        return person;
    }


    public void removePerson(Person person) {
        EntityManager em = ContactsEntityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.remove(em.merge(person));
        tx.commit();
        em.close();
    }
    public Interest addInterest(Interest interest) {
        EntityManager em = ContactsEntityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.persist(interest);
        tx.commit();
        em.close();
        return interest;
    }

    public List<Interest> findAllInterests() {
        EntityManager em = ContactsEntityManagerFactory.createEntityManager();
        List<Interest> interests = em.createQuery("select i from Interest i").getResultList();
        em.close();
        return interests;
    }
}
