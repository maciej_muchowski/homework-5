package pl.codementors.contacts.model;


public enum Gender {
    MALE,
    FEMALE;
}
