package pl.codementors.contacts.model;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "interests")
public class Interest {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private String name;

    @ManyToMany(cascade = CascadeType.ALL, mappedBy = "interests")
    private List<Person> personList;

    public Interest() {
    }

    public Interest(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Person> getPersonList() {
        return personList;
    }

    public void setPersonList(List<Person> personList) {
        this.personList = personList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Interest interest = (Interest) o;
        return id == interest.id &&
                Objects.equals(name, interest.name) &&
                Objects.equals(personList, interest.personList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, personList);
    }

    @Override
    public String toString() {
        return name;
    }
}
