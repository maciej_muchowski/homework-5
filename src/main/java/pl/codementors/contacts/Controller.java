package pl.codementors.contacts;

import javafx.beans.binding.Bindings;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import pl.codementors.contacts.model.Gender;
import pl.codementors.contacts.model.Interest;
import pl.codementors.contacts.model.Person;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    @FXML
    private TableView<Person> personsTable;

    @FXML
    private Button addButton;

    @FXML
    private Button deleteButton;

    @FXML
    private TextField name;

    @FXML
    private TextField surname;

    @FXML
    private TextField gender;

    @FXML
    private TextField phoneNumber;

    @FXML
    private ListView<Interest> interestsList;

    private ObservableList<Person> personObservableList = FXCollections.observableArrayList();

    private ObservableList<Interest> interestObservableList = FXCollections.observableArrayList();

    private StringProperty nameProperty = new SimpleStringProperty();

    private StringProperty surnameProperty = new SimpleStringProperty();

    private StringProperty genderProperty = new SimpleStringProperty();

    private StringProperty phoneNumberProperty = new SimpleStringProperty();

    private ListProperty<Interest> interestListProperty = new SimpleListProperty<>();

    private ContactsDao dao;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        dao = new ContactsDao();
        personsTable.setItems(personObservableList);
        personObservableList.addAll(dao.findAllPersons());

        personsTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            getSelectedPersonDetails();
        });
        bindProperties();
    }


    @FXML
    private void handleAdd(ActionEvent event) {
        Person person = new Person();
        dialogSetFirstName(person);
        dialogSetLastName(person);
        dialogSetPhoneNumber(person);
        dialogSetGender(person);
        dialogSetInterests(person);
        person = dao.addPerson(person);
        personObservableList.add(person);
    }

    @FXML
    private void handleRemove(ActionEvent event) {
        int selectedRow = personsTable.getSelectionModel().getSelectedIndex();
        if (selectedRow >= 0) {
            dao.removePerson(personsTable.getSelectionModel().getSelectedItem());
            personsTable.getItems().remove(selectedRow);
        }
    }

    private void dialogSetFirstName(Person person) {
        TextInputDialog input = new TextInputDialog("");
        input.setHeaderText("Set first name");
        String firstName = input.showAndWait().orElse("");
        person.setFirstName(firstName);
    }

    private void dialogSetLastName(Person person) {
        TextInputDialog input = new TextInputDialog("");
        input.setHeaderText("Set last name");
        String lastName = input.showAndWait().orElse("");
        person.setLastName(lastName);
    }

    private void dialogSetPhoneNumber(Person person) {
        TextInputDialog input = new TextInputDialog("");
        input.setHeaderText("Set phone number");
        String phoneNumber = input.showAndWait().orElse("");
        person.setPhoneNumber(phoneNumber);
    }

    private void dialogSetGender(Person person) {
        List<String> genders = Arrays.asList(Gender.MALE.name(), Gender.FEMALE.name());
        ChoiceDialog<String> choice = new ChoiceDialog<>(null, genders);
        choice.setHeaderText("Select gender");
        String gender = choice.showAndWait().orElse("");
        person.setGender(Gender.valueOf(gender));
    }

    private void dialogSetInterests(Person person) {
// TODO: 21.08.18 setting interests
        ObservableList<Interest> allInterestList = FXCollections.observableArrayList(dao.findAllInterests());
        ChoiceDialog<Interest> choice = new ChoiceDialog<>(null, allInterestList);
        choice.setHeaderText("Select interests");
        Interest interest = choice.showAndWait().orElse(null);
        person.getInterests().add(interest);
    }

    private void getSelectedPersonDetails() {
        Person person = personsTable.getSelectionModel().getSelectedItem();
        nameProperty.set(person.getFirstName());
        surnameProperty.set(person.getLastName());
        genderProperty.set(person.getGender().name());
        phoneNumberProperty.set(person.getPhoneNumber());
        ObservableList<Interest> personInterestObservableList = FXCollections.observableArrayList(person.getInterests());
        interestListProperty.set(personInterestObservableList);
    }

    private void bindProperties() {
        name.textProperty().bindBidirectional(nameProperty);
        surname.textProperty().bindBidirectional(surnameProperty);
        gender.textProperty().bindBidirectional(genderProperty);
        phoneNumber.textProperty().bindBidirectional(phoneNumberProperty);
        interestsList.itemsProperty().bindBidirectional(interestListProperty);
        deleteButton.disableProperty().bind(Bindings.isEmpty(personsTable.getSelectionModel().getSelectedItems()));
    }


}
